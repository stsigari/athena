ApplicationMgr.Dlls += { "StoreGate", "TrigNavigation", "TrigSerializeCnvSvc" };
ApplicationMgr.ExtSvc += { "ClassIDSvc" };
ApplicationMgr.ExtSvc += { "TrigSerializeCnvSvc" };
ApplicationMgr.ExtSvc += { "StoreGateSvc", "StoreGateSvc/DetectorStore", "StoreGateSvc/HistoryStore" };
ApplicationMgr.ExtSvc += { "ActiveStoreSvc" };
ApplicationMgr.ExtSvc += { "ToolSvc" };
AuditorSvc.Auditors  += { "AlgContextAuditor"};
StoreGateSvc.OutputLevel = 0;
StoreGateSvc.ActivateHistory = false;
MessageSvc.useColors        = false;

ToolSvc.Navigation.ClassesToPreregister = {"TestA#EverEmptyButPresent", "TestA#AgainPresentButEmpty", "TestA#","TrigNavTest::TestBContainer#BContainer1", "TrigNavTest::TestBContainer#BContainer2", "TestDContainer#DContainer1"};

ToolSvc.Navigation.OutputLevel=1;
Holder_test.OutputLevel=1;
